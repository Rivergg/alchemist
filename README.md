# Alchemist
Alchemist is a Data miner for the game Go Gigantic by Motiga. The miner is written and Go.

## Downloading and Installing Alchemist

The suggested way to download Alchemist is to have Go installed. If you don't have Go installed, feel free to visit [the Go docs](https://golang.org/doc/install) first.

This method requires that you have Go added to your `PATH` and to have a `GOPATH` set up.

It is actually not simpler than this. Run the following command in your terminal or command line:

```
go get gitlab.com/ggunleashed/alchemist
```
This downloads the files to your `GOPATH` and installs it immediately for you as well.

## Using Alchemist

Now that you have Alchemist installed. You can run alchemist by either typing `bin/alchemist` or `alchemist` in your terminal or command line. The command depends on if you have added `GOPATH/bin` to your `PATH`.

Alchemist should give an error and close, because Alchemist requires some input.

Alchemist has the following flags of which some are required:
```
* -config:   REQUIRED - Location to the config file used for Alchemist.
```

To run Alchemist your command should look something like this:
```
alchemist -config "<config location>"
```

## Alchemist Config file

The config file has the following options:
```
* is_core                   OPTIONAL Boolean  This option is by default set to false. If is set to core, will add this to the json file.
                                              Might have further use in the future as in restricting things from core.

* install_dir               REQUIRED String   Location to the install location for Gigantic.

* output                    OPTIONAL String   Default: If is_core == true: giganticData-Core-<current date+time>.json
                                              Else: giganticData-Live-<current date+time>.json
                                              Is used for the location and file for the output.json.

* previous_data             OPTIONAL String   Used for getting lore from previous data files.
                                              If not set, then nothing happens with lore.

* default_game_location     REQUIRED String   Location of DefaultGame.ini.

* default_skin_location     REQUIRED String   Location of DefaultSkin.ini.

* build_properties_location REQUIRED String   Location of build.properties.

* localization_location     REQUIRED String   Folder with Localization data.

* languages                 OPTIONAL Object   Contains keys that correspond to folder names in the localization folder.
                                              Contains values to keys that are used in the json instead of the folder names.

* skill_upgrades           OPTIONAL Object    Contains keys that correspond to Ini Keys that are used in IniData structs from Machine.
                                              Contains values to keys that are used as names for the skills, like Skill1.

* skill_buttons            OPTIONAL Object    Contains keys that correspond to identifiers in descriptions, like `skill1button.
                                              Contains values to keys that are used to replace the keys with.

* heroes                   OPTIONAL Object    Contains:
         * short_names     OPTIONAL Object    Contains keys that are development names of heroes.
                                              Contains values to keys that are short versions of the actual name of the hero, like Knossos.
```

Example file can be found in:
```
./examples/config.json
```
