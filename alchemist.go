package main

import (
	"encoding/json"
	"flag"
	"fmt"
	"io/ioutil"
	"log"
	"path/filepath"
	"strings"
	"time"

	"gitlab.com/ggunleashed/alchemist/config"
	"gitlab.com/ggunleashed/alchemist/models"
	"gitlab.com/ggunleashed/machine"
)

//GiganticData struct
type GiganticData struct {
	Patch  string        `json:"patch"`
	IsCore bool          `json:"isCore"`
	Heroes []models.Hero `json:"heroes"`
}

func main() {
	startTime := time.Now()
	//setting flags
	configFile := flag.String("config", "", "REQUIRED: Config file location")
	flag.Parse()

	// Initiliaze config
	config := config.New(*configFile)

	//Get Patch Version
	log.Println("Starting to read build data.")
	buildProperties, err := machine.ReadIni(config.BuildPropertiesLocation)
	if err != nil {
		log.Fatal(err)
	}
	patch := buildProperties[0].Data["ChangelistBuiltFrom"]

	localizationPath := config.LocalizationLocation
	localizationDir, _ := ioutil.ReadDir(localizationPath)
	var localizationPathSlice []string
	for _, content := range localizationDir {
		if content.IsDir() {
			contentDir, _ := ioutil.ReadDir(fmt.Sprintf("%s/%s", localizationPath, content.Name()))
			for _, file := range contentDir {
				if strings.Contains(file.Name(), "RxGame") {
					localizationPathSlice = append(localizationPathSlice, fmt.Sprintf("%s/%s/%s", localizationPath, content.Name(), file.Name()))
				}
			}
		}
	}
	localizationIniDataMap := make(map[string][]machine.IniData)
	for _, path := range localizationPathSlice {
		fileExtension := strings.Trim(filepath.Ext(path), ".")
		log.Printf("Starting to read localization: %s. From: %s", fileExtension, path)
		localizationIniData, localeErr := machine.ReadIni(path)

		if localeErr != nil {
			log.Panicln(err)
			continue
		}

		localizationIniDataMap[fileExtension] = localizationIniData
	}
	var heroes []models.Hero
	var skills []models.Skill
	var skillUpgrades []models.SkillUpgrade
	var passiveUpgrades []models.PassiveUpgrade

	log.Println("Starting to read Default Game ini")
	giganticInidata, err := machine.ReadIni(config.DefaultGameIniLocation)

	if err != nil {
		log.Fatal(err)
	}

	for _, iniData := range giganticInidata {
		if strings.EqualFold(iniData.Class, "RxHeroProvider") {
			heroes = append(heroes, models.NewHero(iniData, *config))
		} else if strings.EqualFold(iniData.Class, "RxSkillProvider") {
			skills = append(skills, models.NewSkill(iniData, *config))
		} else if strings.EqualFold(iniData.Class, "RxSkillUpgradeProvider") {
			skillUpgrades = append(skillUpgrades, models.NewSkillUpgrade(iniData, *config))
		} else if strings.EqualFold(iniData.Class, "RxPassiveUpgradeProvider") {
			passiveUpgrades = append(passiveUpgrades, models.NewPassiveUpgrade(iniData, *config))
		}
	}

	log.Println("Starting to read Default Skin ini")
	skinsIniData, err := machine.ReadIni(config.DefaultSkinIniLocation)

	if err != nil {
		log.Fatal(err)
	}

	var heroSkins []models.HeroSkin
	var weaponSkins []models.WeaponSkin
	var skins []models.Skin

	for _, iniData := range skinsIniData {
		if strings.EqualFold(iniData.Class, "RxHeroSkinProvider") {
			heroSkins = append(heroSkins, models.NewHeroSkin(iniData, *config))
		} else if strings.EqualFold(iniData.Class, "RxWeaponSkinProvider") {
			weaponSkins = append(weaponSkins, models.NewWeaponSkin(iniData, *config))
		}
	}

	for language, localization := range localizationIniDataMap {
		language = config.ConfigChangeLocalization(language)
		for _, iniData := range localization {
			if strings.EqualFold(iniData.Class, "RxHeroProvider") {
				for _, hero := range heroes {
					if strings.EqualFold(iniData.Title, hero.HeroArchetypeName) {
						hero.AddLocalization(language, iniData, *config)
					}
				}
			} else if strings.EqualFold(iniData.Class, "RxSkillProvider") {
				for _, skill := range skills {
					if strings.EqualFold(strings.TrimSuffix(iniData.Title, "_Provider"), skill.ResourceID) {
						skill.AddLocalization(language, iniData, *config)
					}
				}
			} else if strings.EqualFold(iniData.Class, "RxSkillUpgradeProvider") {
				for _, skillUpgrade := range skillUpgrades {
					if strings.EqualFold(strings.TrimSuffix(iniData.Title, "Provider"), strings.TrimSuffix(skillUpgrade.ResourceID, "Upgrade")) {
						skillUpgrade.AddLocalization(language, iniData, *config)
					}
				}
			} else if strings.EqualFold(iniData.Class, "RxPassiveUpgradeProvider") {
				for _, passiveUpgrade := range passiveUpgrades {
					if strings.EqualFold(strings.TrimSuffix(iniData.Title, "_Provider"), strings.TrimSuffix(passiveUpgrade.ResourceID, "_Upgrade")) {
						passiveUpgrade.AddLocalization(language, iniData, *config)
					}
				}
			} else if strings.EqualFold(iniData.Class, "RxHeroSkinProvider") {
				for _, heroSkin := range heroSkins {
					if strings.EqualFold(iniData.Title, heroSkin.ResourceID) {
						heroSkin.AddLocalization(language, iniData, *config)
					}
				}
			} else if strings.EqualFold(iniData.Class, "RxWeaponSkinProvider") {
				for _, weaponSkins := range weaponSkins {
					if strings.EqualFold(iniData.Title, weaponSkins.ResourceID) {
						weaponSkins.AddLocalization(language, iniData, *config)
					}
				}
			}
		}
	}

	for _, heroSkin := range heroSkins {
		skinExists := false
		for index := range skins {
			if strings.Contains(config.ChangeHeroShortNameInString(heroSkin.ResourceID), skins[index].ID) {
				skinExists = true
				skins[index].ColourVariantsHeroSkin = append(skins[index].ColourVariantsHeroSkin, heroSkin)
			}
		}
		if !skinExists {
			skins = append(skins, models.NewSkinFromHeroSkin(heroSkin, *config))
		}
	}

	for _, weaponSkin := range weaponSkins {
		skinExists := false
		for index := range skins {
			if strings.Contains(config.ChangeHeroShortNameInString(weaponSkin.ResourceID), skins[index].ID) {
				skinExists = true
				skins[index].ColourVariantsWeaponSkin = append(skins[index].ColourVariantsWeaponSkin, weaponSkin)
			}
		}
		if !skinExists {
			skins = append(skins, models.NewSkinFromWeaponSkin(weaponSkin, *config))
		}
	}

	for _, passiveUpgrade := range passiveUpgrades {
		for _, hero := range heroes {
			if strings.EqualFold(passiveUpgrade.HeroName, hero.ResourceID) {
				hero.AddPassiveUpgrade(passiveUpgrade, *config)
			}
		}
	}

	for _, skillUpgrade := range skillUpgrades {
		for _, skill := range skills {
			if strings.EqualFold(skillUpgrade.HeroName, skill.HeroArchetypeName) {
				if strings.EqualFold(strings.TrimSuffix(strings.TrimPrefix(skillUpgrade.SkillUpgradeCategory, "EUC_"), "Upgrade"), skill.UIResourceID) {
					skill.AddSkillUpgrade(skillUpgrade, *config)
				}
			} else if strings.Contains(skillUpgrade.ResourceID, "Swift") && strings.Contains(skill.ResourceID, "Swift") {
				if strings.Contains(strings.TrimRight(skillUpgrade.ResourceID, "_")[2:8], "SwiftF") {
					if strings.EqualFold(strings.TrimSuffix(strings.TrimPrefix(skillUpgrade.SkillUpgradeCategory, "EUC_"), "Upgrade"), skill.UIResourceID) {
						skill.AddSkillUpgrade(skillUpgrade, *config)
					}
				}
			}
		}
	}

	for _, skill := range skills {
		for _, hero := range heroes {
			if strings.EqualFold(skill.HeroArchetypeName, hero.ResourceID) {
				hero.AddSkill(skill, *config)
			}
		}
	}

	for _, skin := range skins {
		for index := range heroes {
			if strings.EqualFold(skin.HeroName, heroes[index].ResourceID) {
				heroes[index].Skins = append(heroes[index].Skins, skin)
			}
		}
	}

	giganticData := GiganticData{}
	giganticData.Heroes = heroes
	giganticData.Patch = patch
	giganticData.IsCore = config.IsCore

	if !strings.EqualFold(config.PreviousDataLocation, "") {
		previousDataJSON, err := ioutil.ReadFile(config.PreviousDataLocation)
		if err != nil {
			log.Printf("ERROR - Could not find previous data: %s\n", err)
		} else {
			var previousData GiganticData
			json.Unmarshal(previousDataJSON, &previousData)
			AddLoreFromPreviousToNew(&previousData, &giganticData)
		}
	}

	jsonData, _ := json.Marshal(giganticData)
	log.Println("Starting to write json to: " + config.OutputLocation)
	writeErr := ioutil.WriteFile(config.OutputLocation, jsonData, 0777)
	if writeErr != nil {
		log.Fatal(writeErr)
	}
	totalTime := time.Since(startTime)
	fmt.Printf("\n### Alchemist completed in %.3f seconds. ###\n", totalTime.Seconds())
}

// AddLoreFromPreviousToNew Does stuff TODO change all comments
func AddLoreFromPreviousToNew(previous *GiganticData, next *GiganticData) {
	for _, previousHero := range previous.Heroes {
		for _, nextHero := range next.Heroes {
			if strings.EqualFold(previousHero.HeroDisplayName["en"], nextHero.HeroDisplayName["en"]) {
				for language := range previousHero.HeroLore {
					nextHero.HeroLore[language] = previousHero.HeroLore[language]
				}
				for skillName, previousHeroSkill := range previousHero.Skills {
					for language := range previousHeroSkill.SkillLore {
						nextHero.Skills[skillName].SkillLore[language] = previousHeroSkill.SkillLore[language]
					}
				}
				break
			}
		}
	}
}
