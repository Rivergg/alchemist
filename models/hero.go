package models

import (
	"gitlab.com/ggunleashed/alchemist/config"
	"gitlab.com/ggunleashed/machine"
)

//Hero struct
type Hero struct {
	//Localization
	HeroDisplayName map[string]string `json:"fullName"`
	HeroDescription map[string]string `json:"description"`
	HeroFlavorText  map[string]string `json:"flavorText"`
	HeroLore        map[string]string `json:"lore"`

	HeroArchetypeName  string `json:"-"`
	PrimaryTraitPath   string `json:"-"`
	SecondaryTraitPath string `json:"-"`
	ResourceID         string `json:"-"`
	HeroShortName      string `json:"shortName"`
	AttackStat         string `json:"attackStat"`
	DefenseStat        string `json:"defenseStat"`
	MobilityStat       string `json:"mobilityStat"`
	UtilityStat        string `json:"utilityStat"`
	PlayDifficulty     string `json:"playDifficulty"`
	PlayStyle          string `json:"playStyle"`

	//Skills and Talents
	Skills          map[string]Skill            `json:"skills"`
	PassiveUpgrades map[string][]PassiveUpgrade `json:"talents"`
	Skins           []Skin                      `json:"skins"`
}

//NewHero creates new Hero
func NewHero(iniData machine.IniData, config config.Config) (hero Hero) {
	hero.HeroArchetypeName = iniData.Data["HeroArchetypeName"]
	hero.PrimaryTraitPath = iniData.Data["PrimaryTraitPath"]
	hero.SecondaryTraitPath = iniData.Data["SecondaryTraitPath"]
	hero.ResourceID = iniData.Data["ResourceID"]
	hero.HeroShortName = config.ConfigChangeHeroShortName(iniData.Data["ResourceID"])
	hero.AttackStat = iniData.Data["AttackStat"]
	hero.DefenseStat = iniData.Data["DefenseStat"]
	hero.MobilityStat = iniData.Data["MobilityStat"]
	hero.UtilityStat = iniData.Data["UtilityStat"]
	hero.PlayDifficulty = iniData.Data["PlayDifficulty"]
	hero.PlayStyle = iniData.Data["PlayStyle"]

	//Making the maps for Skills and Talents
	hero.Skills = make(map[string]Skill)
	hero.PassiveUpgrades = make(map[string][]PassiveUpgrade)

	//Making the maps for the localizations
	hero.HeroDisplayName = make(map[string]string)
	hero.HeroDescription = make(map[string]string)
	hero.HeroFlavorText = make(map[string]string)
	hero.HeroLore = make(map[string]string)

	return hero
}

func (hero *Hero) AddLocalization(language string, iniData machine.IniData, config config.Config) {
	hero.HeroDisplayName[language] = iniData.Data["HeroDisplayName"]
	hero.HeroDescription[language] = iniData.Data["HeroDescription"]
	hero.HeroFlavorText[language] = iniData.Data["HeroFlavorText"]
}

func (hero *Hero) AddSkill(skill Skill, config config.Config) {
	hero.Skills[skill.SkillName] = skill
}

func (hero *Hero) AddPassiveUpgrade(passiveUpgrade PassiveUpgrade, config config.Config) {
	hero.PassiveUpgrades[passiveUpgrade.MinHeroLevel] = append(hero.PassiveUpgrades[passiveUpgrade.MinHeroLevel], passiveUpgrade)
}
