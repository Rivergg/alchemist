package models

import (
	"strings"

	"gitlab.com/ggunleashed/alchemist/config"
	"gitlab.com/ggunleashed/machine"
)

//SkillUpgrade struct
type SkillUpgrade struct {
	//Localization
	SkillUpgradeDisplayName map[string]string `json:"name"`
	SkillUpgradeDescription map[string]string `json:"description"`

	ResourceID           string `json:"-"`
	SkillUpgradeCategory string `json:"-"`
	SkillIndex           string `json:"-"`
	HeroName             string `json:"-"`
	UpgradeTier          string `json:"-"`
	MinHeroLevel         string `json:"-"`
}

//NewSkillUpgrade creates new SkillUpgrade
func NewSkillUpgrade(iniData machine.IniData, config config.Config) (skillUpgrade SkillUpgrade) {
	skillUpgrade.ResourceID = iniData.Data["ResourceID"]
	skillUpgrade.SkillUpgradeCategory = iniData.Data["SkillUpgradeCategory"]
	skillUpgrade.SkillIndex = iniData.Data["SkillIndex"]
	skillUpgrade.HeroName = iniData.Data["HeroName"]
	skillUpgrade.UpgradeTier = iniData.Data["UpgradeTier"]
	skillUpgrade.MinHeroLevel = iniData.Data["MinHeroLevel"]

	//Making the maps for the localizations
	skillUpgrade.SkillUpgradeDisplayName = make(map[string]string)
	skillUpgrade.SkillUpgradeDescription = make(map[string]string)

	return skillUpgrade
}

func (skillUpgrade *SkillUpgrade) AddLocalization(language string, iniData machine.IniData, config config.Config) {
	skillUpgrade.SkillUpgradeDisplayName[language] = iniData.Data["SkillUpgradeDisplayName"]
	skillUpgrade.SkillUpgradeDescription[language] = iniData.Data["SkillUpgradeDescription"]

	skillUpgrade.SkillUpgradeDescription[language] = strings.Replace(skillUpgrade.SkillUpgradeDescription[language], "`skill1button", config.ConfigChangeSkillButton("`skill1button"), -1)
	skillUpgrade.SkillUpgradeDescription[language] = strings.Replace(skillUpgrade.SkillUpgradeDescription[language], "`skill2button", config.ConfigChangeSkillButton("`skill2button"), -1)
	skillUpgrade.SkillUpgradeDescription[language] = strings.Replace(skillUpgrade.SkillUpgradeDescription[language], "`skill3button", config.ConfigChangeSkillButton("`skill3button"), -1)
	skillUpgrade.SkillUpgradeDescription[language] = strings.Replace(skillUpgrade.SkillUpgradeDescription[language], "`skill4button", config.ConfigChangeSkillButton("`skill4button"), -1)
	skillUpgrade.SkillUpgradeDescription[language] = strings.Replace(skillUpgrade.SkillUpgradeDescription[language], "`focusskillbutton", config.ConfigChangeSkillButton("`focusskillbutton"), -1)
}
